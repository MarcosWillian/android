package com.tsi.marcos.pdm.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.tsi.marcos.pdm.R;
import com.tsi.marcos.pdm.activity.ProjetoActivity;
import com.tsi.marcos.pdm.dao.ProjetoDAO;
import com.tsi.marcos.pdm.model.Projeto;

public class ProjetoEdicaoFragment extends BaseFragment {

    private Projeto projeto; //uma instância da classe Carro com escopo global para utilização em membros da classe
    private final int SAVE = 0;
    private final int DELETE = 1;

    private ProjetoDAO projetoDAO;

    private EditText editTextNome;
    private EditText editTextDescricao;
    private EditText editTextData;
    private Switch editToggleStatus;


    //utilizado pelo Fragment para repassar o objeto carro clicado na lista pelo user
    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //informa ao sistema que o fragment irá adicionar menu na ActionBar
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //infla o layout
        View view = inflater.inflate(R.layout.fragment_edicaoprojeto, container, false);

        //um título para a janela
        ((ProjetoActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_fragment_novoprojeto);

        Log.d(TAG, "Dados do registro = " + projeto); //um log para depurar

                //carrega o tipo nos RadioButtons


        //carrega o nome e a descrição
        Log.d(TAG, "Nome = " + projeto.nome + "\nDescrição = " + projeto.descricao); //um log para depurar
        editTextNome = (EditText) view.findViewById(R.id.inputtitulo_fragmentedicao);
        editTextDescricao = (EditText) view.findViewById(R.id.inputdescricao_fragmentedicao);
        editTextData = (EditText) view.findViewById(R.id.inputdata_fragmentedicao);

        editToggleStatus = (Switch) view.findViewById(R.id.toggle_fragmentedicao);

        if(projeto.status == 1L){
            editToggleStatus.setChecked(true);
        }  else {
            editToggleStatus.setChecked(false);
        }

        editTextNome.setText(projeto.nome);
        editTextDescricao.setText(projeto.descricao);
        editTextData.setText(projeto.prazo);

        return view;
    }

    /*
        Infla o menu.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_edicaoprojeto, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "Entrou no OptionItem");

        switch (item.getItemId()) {
            case R.id.menuitem_salvar:

                projeto.nome      = editTextNome.getText().toString();
                projeto.descricao = editTextDescricao.getText().toString();
                projeto.prazo     = editTextData.getText().toString();

                if(editToggleStatus.isChecked()){
                    projeto.status = 1;
                }
                else{
                    projeto.status = 0;
                }

                projetoDAO.getInstance(getContext()).save(projeto);
                Toast.makeText(getContext(), "Atualizado", Toast.LENGTH_SHORT).show();
                getActivity().finish();
                break;
            case R.id.menuitem_excluir:{
                projetoDAO.getInstance(getContext()).delete(projeto);
                Toast.makeText(getContext(), "Deletado", Toast.LENGTH_SHORT).show();
                getActivity().finish();
                break;
            }
            case android.R.id.home:
                getActivity().finish();
                break;
        }

        return true;
    }

}
