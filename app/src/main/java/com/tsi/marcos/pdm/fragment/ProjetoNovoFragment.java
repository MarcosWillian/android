package com.tsi.marcos.pdm.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.tsi.marcos.pdm.R;
import com.tsi.marcos.pdm.activity.ProjetoActivity;
import com.tsi.marcos.pdm.dao.ProjetoDAO;
import com.tsi.marcos.pdm.model.Projeto;

public class ProjetoNovoFragment extends BaseFragment {

    private Projeto projeto;
    private ProjetoDAO projetoDAO;

    private EditText inputNome; //campo referente ao atributo nome do objeto carro
    private EditText inputDesc; //campo referente ao atributo descrição do objeto carro
    private EditText inputData;

    /* Habilita o menu na activity */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        //((ProjetoActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_fragment_novoprojeto);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_novoprojeto, container, false);

        //um título para a janela
        ((ProjetoActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_fragment_novoprojeto);

        //Cria uma instancia da classe de modelo
        projeto = new Projeto();

        //mapeia e inicializa os componentes da UI
        inputNome = (EditText) view.findViewById(R.id.inputtitulo_fragmentnovo);
        inputDesc = (EditText) view.findViewById(R.id.inputdescricao_fragmentnovo);
        inputData = (EditText) view.findViewById(R.id.inputdata_fragmentnovo);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_novoprojeto, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "Entrou no OptionItem");

        switch (item.getItemId()) {
            case R.id.menuitem_salvar:

                projeto.nome      = inputNome.getText().toString();
                projeto.descricao = inputDesc.getText().toString();
                projeto.prazo     = inputData.getText().toString();
                projeto.status    = 1;

                projetoDAO.getInstance(getContext()).save(projeto);
                Toast.makeText(getContext(), "Salvo", Toast.LENGTH_SHORT).show();
                getActivity().finish();
                break;
            case android.R.id.home:
                getActivity().finish();
                break;
        }

        return true;
    }

}
