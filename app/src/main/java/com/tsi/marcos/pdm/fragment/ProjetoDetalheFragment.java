package com.tsi.marcos.pdm.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tsi.marcos.pdm.R;
import com.tsi.marcos.pdm.activity.ProjetoActivity;
import com.tsi.marcos.pdm.model.Projeto;

public class ProjetoDetalheFragment extends BaseFragment {

    //uma instância da classe Carro com escopo global para utilização em membros da classe
    private Projeto projeto;

    private TextView textViewNome; //campo referente ao atributo nome do objeto carro
    private TextView textViewDescricao; //campo referente ao atributo descrição do objeto carro
    private TextView textViewStatus;  //campo referente ao atributo latitude do objeto carro
    private TextView textViewPrazo; //campo referente ao atributo longitude do objeto carro


    //utilizado pela Activity para repassar o objeto carro clicado na lista pelo user
    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //informa ao sistema que o fragment irá adicionar menu na ActionBar
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //infla o layout
        View view = inflater.inflate(R.layout.fragment_detalheprojeto, container, false);

        //um título para a janela
        ((ProjetoActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_fragment_novoprojeto);

        //carrega o tipo nos RadioButtons
        Log.d(TAG, "Tipo = " + projeto.status); //um log para depurar

        //carrega o nome e a descrição
        Log.d(TAG, "Nome = " + projeto.nome + "\nDescrição = " + projeto.descricao); //um log para depurar
        textViewNome = (TextView) view.findViewById(R.id.inputtitulo_fragmentdetalhe);
        textViewDescricao = (TextView) view.findViewById(R.id.inputdescricao_fragmentdetalhe);
        textViewNome.setText(projeto.nome);
        textViewDescricao.setText(projeto.descricao);

        //carrega a data e o status
        /*
        textViewPrazo  = (TextView) view.findViewById(R.id.);
        textViewStatus = (TextView) view.findViewById(R.id.);
        textViewPrazo.setText(projeto.prazo);
        textViewStatus.setText(projeto.status);
        */

        return view;
    }

    /*
        Infla o menu.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_detalhecarro, menu);
    }




}
