package com.tsi.marcos.pdm.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsi.marcos.pdm.R;
import com.tsi.marcos.pdm.activity.ProjetoActivity;
import com.tsi.marcos.pdm.activity.ProjetosActivity;
import com.tsi.marcos.pdm.adapter.ProjetosAdapter;
import com.tsi.marcos.pdm.dao.ProjetoDAO;
import com.tsi.marcos.pdm.model.Projeto;

import java.util.List;

public class ProjetosFragment extends BaseFragment implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayoutManager;
    private List<Projeto> projetos;
    private ProjetoDAO projetoDAO;

    /* Habilita o menu na activity */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        ((ProjetosActivity) getActivity()).getSupportActionBar().setTitle("Projetos");

        Log.d(TAG, "chamou onCreate() em ProjetosFragment");

        /* Intancia o banco de dados (Executa o metodo OnCreate) */
        projetoDAO = ProjetoDAO.getInstance(getContext());
    }

    /* Lista os projetos */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_projetos, container, false);

        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview_fragmentprojetos);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setHasFixedSize(true);

        return view;

        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_carros, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.menuitem_pesquisar).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.hint_pesquisar));


    }

    @Override
    public void onResume() {
        super.onResume();
        projetos = projetoDAO.getAll();
        recyclerview.setAdapter(new ProjetosAdapter(getContext(), projetos, onClickProjeto())); //Context, fonte de dados, tratador do evento onClick

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    /*
        Este método utiliza a interface declarada na classe CarrosAdapter para tratar o evento onClick do item da lista.
     */
    protected ProjetosAdapter.ProjetoOnClickListener onClickProjeto() {

        Log.d(TAG, "Acionou o evento de Click do projeto");

        //chama o contrutor da interface (implícito) para criar uma instância da interface declarada no adaptador.
        return new ProjetosAdapter.ProjetoOnClickListener() {

            // Aqui trata o evento onItemClick.
            @Override
            public void onClickProjeto(View view, int idx) {
                Projeto projeto = projetos.get(idx);

                Log.d(TAG, "Redirecionou para o projeto detalhe");

                //chama outra Activity para detalhar ou editar o carro clicado pelo usuário
                Intent intent = new Intent(getContext(), ProjetoActivity.class); //configura uma Intent explícita
                intent.putExtra("projeto", projeto); //inseri um extra com a referência para o objeto Carro
                intent.putExtra("qualFragmentAbrir", "ProjetoEdicaoFragment");
                startActivity(intent);
            }

        };
    }

}
