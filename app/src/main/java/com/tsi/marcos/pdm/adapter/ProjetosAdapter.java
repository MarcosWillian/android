package com.tsi.marcos.pdm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tsi.marcos.pdm.R;
import com.tsi.marcos.pdm.model.Projeto;

import java.util.List;

/**
 * Esta classe realiza a adaptação dos dados entre a RecyclerView <-> List.
 * Neste projeto a List está sendo alimentada com dados oriundos de um banco de dados SQLite.
 * @author Vagner Pinto da Silva
 */
public class ProjetosAdapter extends RecyclerView.Adapter<ProjetosAdapter.ProjetosViewHolder> {
    protected static final String TAG = "ProjetosAdapter";
    private final List<Projeto> projetos;
    private final Context context;

    private ProjetoOnClickListener ProjetoOnClickListener;

    public ProjetosAdapter(Context context, List<Projeto> projetos, ProjetoOnClickListener gameOnClickListener) {
        this.context = context;
        this.projetos = projetos;
        this.ProjetoOnClickListener = gameOnClickListener;
    }

    @Override
    public int getItemCount() {
        return this.projetos != null ? this.projetos.size() : 0;
    }

    @Override
    public ProjetosViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Infla a view do layout
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_projetos, viewGroup, false);

        // Cria o ViewHolder
        ProjetosViewHolder holder = new ProjetosViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProjetosViewHolder holder, final int position) {
        // Atualiza a view
        Projeto p = projetos.get(position);
        Log.d(TAG, "Projeto no Adapter da RecyclerView: " + p.toString());

        Log.d(TAG, p.toString());

        holder.hNome.setText(p.nome);
        holder.hPrazo.setText(p.prazo);

        // Click
        if (ProjetoOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Entrou no onClick do adapter.");
                    ProjetoOnClickListener.onClickProjeto(holder.itemView, position); // A variável position é final
                }
            });
        }


    }

    public interface ProjetoOnClickListener {
        public void onClickProjeto(View view, int idx);
    }

    // ViewHolder com as views
    public static class ProjetosViewHolder extends RecyclerView.ViewHolder {

        public TextView hNome;
        public TextView hPrazo;

        public ProjetosViewHolder(View view) {
            super(view);
            // Cria as views para salvar no ViewHolder

            hNome   = (TextView) view.findViewById(R.id.titulo_projeto_adapterProjetos);
            hPrazo  = (TextView) view.findViewById(R.id.prazo_projeto_adapterProjetos);

        }
    }



}