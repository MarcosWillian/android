package com.tsi.marcos.pdm.model;

import java.io.Serializable;

public class Projeto implements Serializable {

    private static final long serialVersionUID = 1L;

    public Long    id;
    public String  nome;
    public String  descricao;
    public String  prazo;
    public Integer status;

    @Override
    public String toString() {
        return "Projeto{"
                + "id='" + id + '\''
                + ", nome='" + nome + '\''
                + ", descricao='" + descricao + '\''
                + ", prazo='" + prazo + '\''
                + ", status='" + status + '\''
                + '}';
    }


}
