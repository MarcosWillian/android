package com.tsi.marcos.pdm.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.tsi.marcos.pdm.model.Projeto;

import java.util.ArrayList;
import java.util.List;

public class ProjetoDAO extends SQLiteOpenHelper {

    private static String TAG  = "bdprojetos";
    private static String NAME = "projeto.sqlite";
    private static int VERSION = 1;
    private static ProjetoDAO projetoDAO = null;

    /* #### Construtor #### */
    private ProjetoDAO(Context context) {
        super(context, NAME, null, VERSION);
        Log.d(TAG, "chamou ProjetoDAO() em ProjetoDAO");
        getWritableDatabase(); //abre a conexão com o bd, utilizado pelo onCreate()
    }

    //Singleton
    public static ProjetoDAO getInstance(Context context){
        if(projetoDAO == null){
            return projetoDAO = new ProjetoDAO(context);
        }

        return projetoDAO;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG, "chamou onCreate() em ProjetoDAO");

        //cria a tabela
        String sql = "create table if not exists projeto " +
                "(_id integer primary key autoincrement, " +
                "nome text, " +
                "descricao text, " +
                "prazo date, " +
                "status integer);";
        Log.d(TAG, "Criando a tabela projeto. Aguarde ...");
        sqLiteDatabase.execSQL(sql);
        Log.d(TAG, "Tabela projeto criada com sucesso.");
        new Task().execute(); //popula o bd
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /* ###########################> CRUD <################################ */


    public List<Projeto> getAll(){
        //abre a conexão com o bd
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        try {
            return toList(sqLiteDatabase.rawQuery("select * from projeto", null));
        }finally {
            sqLiteDatabase.close(); //libera o recurso
        }

    }

    private List<Projeto> toList(Cursor cursor) {
        List<Projeto> projetos = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Projeto projeto = new Projeto();

                // recupera os atributos do cursor para o carro
                projeto.id        = cursor.getLong(cursor.getColumnIndex("_id"));
                projeto.nome      = cursor.getString(cursor.getColumnIndex("nome"));
                projeto.descricao = cursor.getString(cursor.getColumnIndex("descricao"));
                projeto.prazo     = cursor.getString(cursor.getColumnIndex("prazo"));
                projeto.status    = cursor.getInt(cursor.getColumnIndex("status"));

                projetos.add(projeto);

            } while (cursor.moveToNext());
        }

        return projetos;
    }

    public long save(Projeto projeto){

        SQLiteDatabase db = getWritableDatabase(); //abre a conexão com o banco

        try{
            //tupla com: chave, valor
            ContentValues values = new ContentValues();
            values.put("nome", projeto.nome);
            values.put("descricao", projeto.descricao);
            values.put("prazo", projeto.prazo);
            values.put("status", projeto.status);

            //realiza a operação
            if(projeto.id == null){
                //insere no banco de dados
                return db.insert("projeto", null, values);
            }else{
                //altera no banco de dados
                values.put("_id", projeto.id);
                return db.update("projeto", values, "_id=" + projeto.id, null);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            db.close(); //libera o recurso
        }

        return 0L; //caso não realize a operação
    }


    public long delete(Projeto projeto){
        SQLiteDatabase db = getWritableDatabase(); //abre a conexão com o banco
        try{
            return db.delete("projeto", "_id=?", new String[]{String.valueOf(projeto.id)});
        }
        finally {
            db.close(); //libera o recurso
        }
    }


    //Thread para executar a inserção de dados no bd.
    //Utilizada apenas na criação do bd
    private class Task extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            return popularTabelaProjeto();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                Log.d(TAG, "Tabela projeto populada com sucesso.");
            }
        }

        //popula a tabela projeto
        private boolean popularTabelaProjeto() {
            //abre a conexão com o bd
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            try {
                //registro 1
                ContentValues values = new ContentValues();
                values.put("nome", "Projeto TCC");
                values.put("descricao", "Descrição do projeto TCC");
                values.put("prazo", "12/07/2018");
                values.put("status", 1);
                sqLiteDatabase.insert("projeto", null, values);

                //registro 2
                values = new ContentValues();
                values.put("nome", "Projeto PDM");
                values.put("descricao", "Descrição do projeto PDM");
                values.put("prazo", "20/07/2018");
                values.put("status", 1);
                sqLiteDatabase.insert("projeto", null, values);

            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            } finally {
                sqLiteDatabase.close(); //libera o recurso
            }

            return true;
        }

    }

}
