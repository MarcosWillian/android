package com.tsi.marcos.pdm.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tsi.marcos.pdm.R;
import com.tsi.marcos.pdm.fragment.ProjetoDetalheFragment;
import com.tsi.marcos.pdm.fragment.ProjetoEdicaoFragment;
import com.tsi.marcos.pdm.fragment.ProjetoNovoFragment;
import com.tsi.marcos.pdm.model.Projeto;

public class ProjetoActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_projeto);

        /*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_projeto);
        setSupportActionBar(toolbar);
        */

        String msg = (String) getIntent().getCharSequenceExtra("qualFragmentAbrir");
        if(msg.equals("ProjetoNovoFragment")){
            replaceFragment(R.id.fragment_container, new ProjetoNovoFragment());
        }
        else if(msg.equals("ProjetoDetalheFragment")){

            //constrói uma instância do Fragment ProjetoDetalheFragment
            ProjetoDetalheFragment projetoDetalheFragment = new ProjetoDetalheFragment();

            //insere o fragmento como conteúdo de content_main.xml
            replaceFragment(R.id.fragment_container, projetoDetalheFragment);

            //obtém o carro que foi repassado pela CarrosActivity ao chamar esta Activity
            Projeto projeto = (Projeto) getIntent().getSerializableExtra("projeto");
            Log.d(TAG, "Objeto projeto recebido: " + projeto.toString()); //um log para o LogCat

            //repassa o objeto projeto para o fragmento
            projetoDetalheFragment.setProjeto(projeto);

        }
        else if(msg.equals("ProjetoEdicaoFragment")){

            //constrói uma instância do Fragment ProjetoDetalheFragment
            ProjetoEdicaoFragment projetoEdicaoFragment = new ProjetoEdicaoFragment();

            //insere o fragmento como conteúdo de content_main.xml
            replaceFragment(R.id.fragment_container, projetoEdicaoFragment);

            //obtém o carro que foi repassado pela CarrosActivity ao chamar esta Activity
            Projeto projeto = (Projeto) getIntent().getSerializableExtra("projeto");
            Log.d(TAG, "Objeto projeto recebido: " + projeto.toString()); //um log para o LogCat

            //repassa o objeto projeto para o fragmento
            projetoEdicaoFragment.setProjeto(projeto);

        }
    }


}
